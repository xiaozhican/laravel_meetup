<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class,2)->create();
        $user = \App\User::find(1);
        $user->name = 'admin';
        $user->email = '1073298557@qq.com';
        $user->save();
    }
}
