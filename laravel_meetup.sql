/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : laravel_meetup

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-12-25 17:11:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES ('1', '6', 'asdfasdf', '2019-12-18 03:19:13', '2019-12-18 03:19:13', '4');
INSERT INTO `comments` VALUES ('2', '6', 'laravel真有意思', '2019-12-25 16:16:30', '2019-12-25 16:16:30', '4');
INSERT INTO `comments` VALUES ('3', '6', '@ruby 学无止境', '2019-12-25 16:56:42', '2019-12-25 16:56:42', '4');
INSERT INTO `comments` VALUES ('4', '6', '> laravel真不错', '2019-12-25 17:00:52', '2019-12-25 17:00:52', '4');

-- ----------------------------
-- Table structure for commets
-- ----------------------------
DROP TABLE IF EXISTS `commets`;
CREATE TABLE `commets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of commets
-- ----------------------------

-- ----------------------------
-- Table structure for issues
-- ----------------------------
DROP TABLE IF EXISTS `issues`;
CREATE TABLE `issues` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of issues
-- ----------------------------
INSERT INTO `issues` VALUES ('1', 'PHP lover', '2019-12-18 02:10:51', '2019-12-18 02:14:29', 'The PHP Framework For Web Artisans', '4');
INSERT INTO `issues` VALUES ('2', 'Rails and laravel', '2019-12-18 02:10:56', '2019-12-18 02:15:09', 'The PHP Migrate you counld', '4');
INSERT INTO `issues` VALUES ('3', 'Laravel_meetup', '2019-12-18 02:42:25', '2019-12-18 02:42:25', 'Laravel_meetup', '6');
INSERT INTO `issues` VALUES ('4', 'php1', '2019-12-18 02:44:43', '2019-12-18 02:44:43', 'php1', '7');
INSERT INTO `issues` VALUES ('5', 'php2', '2019-12-18 02:44:51', '2019-12-18 02:44:51', 'php2', '6');
INSERT INTO `issues` VALUES ('6', 'laravel框架', '2019-12-18 02:45:01', '2019-12-18 11:27:50', '工匠们使用的框架', '7');
INSERT INTO `issues` VALUES ('7', 'oh', '2019-12-25 15:27:21', '2019-12-25 15:27:21', 'on', '4');

-- ----------------------------
-- Table structure for issues2
-- ----------------------------
DROP TABLE IF EXISTS `issues2`;
CREATE TABLE `issues2` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of issues2
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2019_12_18_014638_create_issues_table', '1');
INSERT INTO `migrations` VALUES ('4', '2019_12_18_020642_create_issues_table', '2');
INSERT INTO `migrations` VALUES ('5', '2019_12_18_021126_add_content_to_issues_table', '2');
INSERT INTO `migrations` VALUES ('6', '2019_12_18_030054_create_commets_table', '2');
INSERT INTO `migrations` VALUES ('7', '2019_12_25_110831_add_username_to_users_table', '2');
INSERT INTO `migrations` VALUES ('8', '2019_12_25_134508_add_socialites_to_users_table', '3');
INSERT INTO `migrations` VALUES ('9', '2019_12_25_152220_add_user_id_to_issues_table', '4');
INSERT INTO `migrations` VALUES ('10', '2019_12_25_161010_change_attributes_in_comments', '5');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
INSERT INTO `password_resets` VALUES ('1073298557@qq.com', '$2y$10$LE/.KVbB34Kh3jPASq1ic./oDb5p6fDzYfduioGsmrWcuSZIfXo1y', '2019-12-25 11:39:17');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Angie Beahan', 'reginald08@example.net', '2019-12-25 11:18:18', '$2y$10$rd/MUgK.azsq30crCVdwROhQRYfcjNC7STJAJvUW1DNV30UMOJyMO', 'iwBhNwgEi8', '2019-12-25 11:18:18', '2019-12-25 11:18:18', 'admin', null, null, null);
INSERT INTO `users` VALUES ('4', 'admin', '1073298557@qq.com', '2019-12-25 11:20:48', '$2y$10$O.smOwPgWR5taVOXJM7EvuiiChCvjX8Y.gifprnkabE4mjrFbfN4y', 'W60xSIbiQPb8GmAvCaOqcanWquIaIz6yvT29IC8WJa7Lo96KBaoum7e1JCZK', '2019-12-25 11:20:48', '2019-12-25 11:20:48', 'admin888', null, null, null);
INSERT INTO `users` VALUES ('6', 'xiaozhican', 'github+24653417@example.com', null, '$2y$10$mCi3WMT8oK6d1/7T00AGK.BxTeldFKxGSOrZY6kTRvExqF4b0.pzu', 'Jyl9yBqmCeg0vi4zeXMM3AXbOGghxYVdd6Xb8ClNDA8oqPRp7vh9TRm7AroM', '2019-12-25 14:36:48', '2019-12-25 14:36:48', null, 'github', '24653417', 'https://avatars0.githubusercontent.com/u/24653417?v=4');
INSERT INTO `users` VALUES ('7', 'ruby', 'ruby@qq.com', null, '$2y$10$zopr8n3myEE2ALHKy/kbeexS3p0TbO6ryKJ5XPFnL0V7nwFgxMgze', null, '2019-12-25 15:11:04', '2019-12-25 15:11:04', 'ruby', null, null, '/storage/images/EzrugBRuUD86auro7qFvkC2c5n8xq0qoxcsxqxKa.jpeg');
