<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Socialite;
use Illuminate\Support\Str;

class SocialitesController extends Controller
{
    //跳转到github授权页面
    public function github()
    {
        return Socialite::with('github')->redirect();
    }

    //用户授权后，跳转回来
    public function callback()
    {
        $info = Socialite::driver('github')->user();
        $user = User::where('provider','github')->where('uid',$info->id)->first();
        if(!$user){
            $user = User::create([
                'provider' => 'github',
                'uid' => $info->id,
                'email' => 'github+' . $info->id . '@example.com',
                'password' => bcrypt(Str::random(10)),
                'name' => $info->nickname,
                'avatar' => $info->avatar,
            ]);
        }
        Auth::login($user,true);
        return redirect('/');
    }
}
